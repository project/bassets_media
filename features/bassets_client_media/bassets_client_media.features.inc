<?php
/**
 * @file
 * bassets_client_media.features.inc
 */

/**
 * Implements hook_views_api().
 */
function bassets_client_media_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
